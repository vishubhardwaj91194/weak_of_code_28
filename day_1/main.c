#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int main(){
    int n; 
    int c; 
    int m;
    int max_people = -1;
    scanf("%d %d %d",&n,&c,&m);
    int *p = malloc(sizeof(int) * n);

    // read input
    for(int p_i = 0; p_i < n; p_i++){
       scanf("%d",&p[p_i]);
    }

    // calculate max people in all trips
    for (int p_i = 0; p_i < n; p_i++) {
        if (p[p_i] > max_people) {
            max_people = p[p_i];
        }
    }
    
    // check if capacity is less than max people
    if (max_people > c * m) {
        printf("No");
    } else {
        printf("Yes");
    }
    return 0;
}
